export const environment = {
  production: true,
  backendUrl: 'https://factory1.brandadrian.ch',
  appVersion: require('../../package.json').version,
};