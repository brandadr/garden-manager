import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Device, DeviceCommand, DeviceInformation } from "factory-device-manager"

@Injectable({
    providedIn: 'root'
})
export class DeviceManagerService {

    private readonly url: string = `${environment.backendUrl}/api/gardenmanager/devices`;

    constructor(private http: HttpClient) { }

    public getDevices(): Observable<Array<Device>> {

        return this.http.get<Array<Device>>(this.url);
    }

    public writeDevice(deviceCommand: DeviceCommand): Observable<any> {

        return this.http.put<any>(`${this.url}/update` , deviceCommand);
    }

    public getDeviceInformation(): Observable<Array<DeviceInformation>> {

        return this.http.get<Array<DeviceInformation>>(`${this.url}/information`);
    }

    public getDeviceInformationById(id: number): Observable<DeviceInformation> {

        return this.http.get<DeviceInformation>(`${this.url}/information/${id}`);
    }
}