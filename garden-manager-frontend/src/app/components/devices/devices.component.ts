import { Component, OnInit } from '@angular/core';
import { DeviceCommand, DeviceManagerItem } from 'factory-device-manager';
import { Observable, of, Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { DeviceManagerService } from '../../services/device-manager.service';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss']
})
export class DevicesComponent implements OnInit {
  public deviceManagerItems$: Observable<Array<DeviceManagerItem>> = of([]);
  public commandChange: Subject<DeviceCommand> = new Subject();
  public commandExecute: Subject<boolean> = new Subject();
  public commandExecute$: Observable<boolean> = this.commandExecute.asObservable();

  constructor(
    public readonly deviceManagerService: DeviceManagerService
  ) { }

  public ngOnInit(): void {
    this.load();

    this.commandChange.pipe(tap(device => {
      this.commandExecute.next(true);

      this.deviceManagerService.writeDevice(device).subscribe(() => {
        this.load();
      });
    })).subscribe(() => {
      this.commandExecute.next(false);
    });
  }

  private load(): void {
    this.deviceManagerItems$ = this.deviceManagerService.getDevices().pipe(map(devices => {
      let items = [];

      for (let device of devices) {
        let deviceManagerItem: DeviceManagerItem = {
          Device: device,
          DeviceInformation: this.deviceManagerService.getDeviceInformationById(device.Id).pipe(map(deviceInformation => deviceInformation)),
          IsReadOnly: false,
          IsLoading: false,
          executeCommand: (command: DeviceCommand) => this.deviceManagerService.writeDevice(command), 
          getDeviceInformation: () => this.deviceManagerService.getDeviceInformationById(device.Id).pipe(map(deviceInformation => deviceInformation))
        }

        items.push(deviceManagerItem)
      }

      return items;
    }));
  }
}
