import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DevicesComponent } from './components/devices/devices.component';

const routes: Routes = [
  {
    path: '',
    component: DevicesComponent,
    pathMatch: 'full'
  },
  {
    path: 'analysis',
    component: DashboardComponent,
    pathMatch: 'full'
  },
  {
    path: 'devices',
    component: DevicesComponent,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
